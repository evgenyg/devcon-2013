
@Grab( 'com.google.guava:guava:14.0-rc2' )
import com.google.common.io.ByteStreams
import com.google.common.io.InputSupplier
import java.util.zip.Adler32
import groovyx.gpars.GParsPool


/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Validates pages checksum to notify of new versions released.
 * URLs map: key   - URL to watch
 *           value - one or more elements list:
 *                   [0]    - (required) page checksum
 *                   [1..N] - (optional) regexes of page elements to remove before calculating the checksum
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * May become outdated as new versions are released. See up-to-date version at
 * https://github.com/evgeny-goldin/scripts/blob/master/src/main/groovy/check-versions/checkVersions.groovy
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


final URLs = [
    'http://confluence.jetbrains.net/display/TW/Previous+Releases+Downloads'       : [ 2277666298, /(?s).+<div class="wiki-content">/,
                                                                                                   /(?s)<rdf:RDF xmlns:rdf.+/ ],
    'http://www.jetbrains.com/youtrack/download/get_youtrack.html'                 : [ 3546232234, /(?s).+?<dt>WAR<\/dt>/,
                                                                                                   /(?s)<dt class="gray">.+/ ],
    'http://repository.jetbrains.com/kotlin/org/jetbrains/kotlin/kotlin-compiler/' : [ 2352484388, /(\d\d-\w+-\d{4} \d\d:\d\d)|(\d+ bytes)/,
                                                                                                   /(Artifactory\/\d+\.\d+\.\d+)/ ],
    'http://services.gradle.org/distributions'                                     : [ 1158170063, /(?s)^.+?<ul class="items">/,
                                                                                                   /(?s)<\/ul>.+$/ ],
    'http://sourceforge.net/projects/codenarc/files/codenarc/'                     : [ 3971608245, /(?s)^.+Looking for the latest version/,
                                                                                                   /(?s)<div id="files-sidebar">.+/,
                                                                                                   /<td headers="files_status_h" class="status folder">.+?<\/td>/,
                                                                                                   /(?s)<div id="sidebar-ads">.+/,
                                                                                                   /document.write\(.+?\)/,
                                                                                                   /(?s)<footer id="site-copyright-footer">.+?<\/footer>/,
                                                                                                   /<script src="http:\/\/a.fsdn.com.+?<\/script>/,
                                                                                                   /<script type="text\/javascript" src="http:\/\/a.fsdn.com.+?<\/script>/ ]
]


GParsPool.withPool {

    final checksum = { String text ->
        ByteStreams.getChecksum({ new ByteArrayInputStream( text.getBytes( 'UTF-8' )) } as InputSupplier, new Adler32())
    }

    URLs.eachParallel {
        String url, List data ->

        final oldChecksum = data.head()
        final text        = data.tail().inject( url.toURL().getText( 'UTF-8' )) { String s, String regex -> s.replaceAll( regex, '' )}
        final newChecksum = checksum( text )

        assert newChecksum == oldChecksum, "URL [$url] checksum has changed to [$newChecksum]"
        println "[${ Thread.currentThread() }][$url] - no changes"
    }
}

