
if ([ 'ftp://', 'scp://', 'http://' ].any { 'http://host'.startsWith( it ) }) { println '1' }

if ([ 'a', 'b', 'c', 'd' ].any{ it == 'd' }) { println '2' }

if ([ 'a', 'b', 'c', 'd' ].grep( 'd' )) { println '3' }

if ([ 'a', 'b', 'c', 'd' ].every{ it != 'q' }) { println '4' }

assert [ 1, '2', [ 'a' ], [ 1 : 2 ]].every{ it }

[ 1, '2', [ 'a' ], [ 1 : 2 ]].each { assert it } // Better error message when fails
