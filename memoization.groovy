
final timer = {
    Closure c ->
    final t = System.currentTimeMillis()
    c()
    println "[${ System.currentTimeMillis() - t }] ms"
}

def fib1; fib1 = { int n -> ( n < 2 ) ? n : fib1( n - 1 ) + fib1( n - 2 ) }
def fib2; fib2 = { int n -> ( n < 2 ) ? n : fib2( n - 1 ) + fib2( n - 2 ) }.memoize()
def fib3; fib3 = { int n -> ( n < 2 ) ? n : fib3( n - 1 ) + fib3( n - 2 ) }.memoizeAtMost ( 2 )
def fib4; fib4 = { int n -> ( n < 2 ) ? n : fib4( n - 1 ) + fib4( n - 2 ) }.memoizeAtLeast( 2 )

timer{ fib1( 36 )}
timer{ fib2( 36 )}
timer{ fib3( 36 )}
timer{ fib4( 36 )}
assert fib2( 36 ) == 14930352

//timer{ fib4( 1000 )} // Throws StackOverflowError
