
final timer = {
    Closure c ->
    final t = System.currentTimeMillis()
    c()
    println "[${ System.currentTimeMillis() - t }] ms"
}


class Math
{
    static BigInteger fib1 ( int N )
    {
        BigInteger result = 0
        BigInteger next   = 1

        N.times {
            BigInteger temp = next + result
            next            = result
            result          = temp
        }

        result
    }
}


def fib2; fib2 = { int n, result = BigInteger.ZERO, next = BigInteger.ONE ->
                   ( n < 1 ) ? result : fib2( n - 1, next, result + next ) }


def fib3; fib3 = { int n, result = BigInteger.ZERO, next = BigInteger.ONE ->
                   ( n < 1 ) ? result : fib3.trampoline( n - 1, next, result + next ) }.trampoline()


final printFib = { int n, Closure fib ->
                   final result = fib( n )
                   println "fib($n) - [${ result.toString().size() }]-[$result]" }


timer{ printFib( 1000,   Math.&fib1 )}
//timer{ printFib( 1000,   fib2 )} // Throws StackOverflowError
timer{ printFib( 1000,   fib3 )}

timer{ printFib( 200000, Math.&fib1 )}
timer{ printFib( 200000, fib3 )}