import groovyx.gpars.GParsPool

GParsPool.withPool {

    final timer = {
        Closure c ->
        final t = System.currentTimeMillis()
        c()
        println "[${ System.currentTimeMillis() - t }] ms"
    }

    timer {
        'http://lenta.ru/sitemap.xml'.
        toURL().text.readLines()*.
        trim().
        findAllParallel{ String s -> s.startsWith( '<loc>' ) && s.endsWith( '</loc>' )}.
        collectParallel{ String s -> s[ 5 .. -7 ] }.
        findAllParallel{ String s -> s.contains( '/sport/' ) }.
        eachParallel   { String s -> s.toURL().text; println "[$s]" }
    }
}
